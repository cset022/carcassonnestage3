package code;

/**
 * Constant variables for the CarcassonneStage3 project.
 * 
 * @author Paul Go
 * @author Andrew Kim
 * @author Jason Caserta
 * @author Mike Albanese
 */
public class CarcassonneConstants {

	/**
	 * The dimensions of the board.
	 */
	public static final short BOARD_DIMENSIONS = 168;
	
	/**
	 * The starting indices.
	 */
	public static final byte STARTING_TILE_INDEX = 83;
}
